/**
 * ripple.js
 * @zzy
 * @date    2015-01-05 15:35:57
 * @version 1.000
 */

//绘制圆形波纹
var drawRipple = function(temp, wave) {
	var maxRadius = Math.max(temp.height(), temp.width()),
		width = wave.width(),
		height = wave.height(),
		range = (maxRadius - Math.max(wave.width(), wave.height())) / 1000 * 16.7,
		opacity = 1,
		start = 0,
		during = 1000 / 16.7,
		loop = function() {
			start += 1;
			var newRange = Tween.Circ.easeOut(start, 0, range, during);
			console.log(newRange);
			var pos = wave.position();
			wave.css({
				top: pos.top - newRange / 2,
				left: pos.left - newRange / 2,
				width: width += newRange,
				height: height += newRange,
				opacity: opacity -= newRange / (maxRadius - width)
			});
			if (start < during) {
				requestAnimationFrame(loop);
			} else {
				temp.remove();
			}
		};
	loop();
};
//点击事件

$(".ripple").on('mousedown', function(e) {
	var me = $(this),
		eventPos = [e.offsetX, e.offsetY],
		temp = $('<div class="ripple-content"><div class="ripple-bg"></div><div class="ripple-waves"><div class="ripple-wave"></div></div></div>');
	me.prepend(temp);
	var wave = temp.find('.ripple-wave'),
		colorR = parseInt(Math.random() * 255),
		colorG = parseInt(Math.random() * 255),
		colorB = parseInt(Math.random() * 255);
	wave.css({
		top: eventPos[1] - wave.height() / 2,
		left: eventPos[0] - wave.width() / 2,
		background: "rgba(" + colorR + "," + colorG + "," + colorB + ",.5" + ")"
	});
	drawRipple(temp, wave);
});